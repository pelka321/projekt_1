#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "statystyka.h"

struct DaneStatystyczne
{
    float X;
    float Y;
    float RHO;
};

int zlicz_ilosc_wierszy_danych_w_pliku(char* sciezkaDoPliku)
{
    FILE *plik = fopen(sciezkaDoPliku, "r");
    if (plik != NULL)
    {
        int wynik = 0;
        char linia[1000] = "";

        while (fgets(linia, 1000, plik) != NULL)
        {
            linia[strlen(linia) - 1] = '\0';

            // sprawdzenie czy we wczytanej linii tekstu sa w ogole jakies dane (pomijanie pustych linii)
            if (strlen(linia) > 0)
                wynik++;
        }

        fclose(plik);
        return wynik;
    }

    return -1;
}

bool wczytaj_dane_z_pliku(char* sciezkaDoPliku, struct DaneStatystyczne* dane)
{
    FILE *plik = fopen(sciezkaDoPliku, "r");
    if (plik != NULL)
    {
        char sLp[10] = "";
        char sX[10] = "";
        char sY[10] = "";
        char sRHO[10] = "";

        if (fscanf(plik, "%s %s %s %s", sLp, sX, sY, sRHO) != EOF)
        {
            int i = 0;
            float x = 0.0;
            float y = 0.0;
            float rho = 0.0;

            while (fscanf(plik, "%s %f %f %f", sLp, &x, &y, &rho) != EOF)
            {
                if (strlen(sLp) > 0)
                {
                    struct DaneStatystyczne d = { x, y, rho };
                    dane[i++] = d;
                }
            }
        }
        fclose(plik);
        return true;
    }

    return false;
}

bool zapisz_dane_do_pliku(char* sciezkaDoPliku, float wynikX, float wynikY, float wynikRHO, int lp)
{
    FILE *plik = fopen(sciezkaDoPliku, "r+");
    if (plik != NULL)
    {
        // Przejscie na koniec pliku.
        fseek(plik, 0, SEEK_END);

        // Cofniecie sie o jeden bajt w pliku, aby sprawdzic, czy ostatni znak w tym pliku to zlamanie linii (enter).
        fseek(plik, -1L, SEEK_CUR);

        // Jesli ostatni znak w pliku to zlamanie linii, to znowu cofniecie sie o jeden bajt,
        // poniewaz getc juz spowodowalo przejscie o jeden znak (bajt) do przodu. Cofniecie sie o znak
        // spowoduje nadpisanie entera enterem podczas pierwszego wpisu do pliku. Chodzi tu o to, aby
        // zawsze miec pewnosc, ze dane zostana wpisane do nowej linii, niezaleznie jak dane sie koncza
        // w pliku (enterem czy tez nie).
        if (getc(plik) == '\n')
        {
            fseek(plik, -1L, SEEK_CUR);
        }

        // Potwierdzenie biezacej pozycji w pliku.
        fseek(plik, 0, SEEK_CUR);

        fprintf(plik, "\n%d.\t%.5f		%.5f	%.3f", lp, wynikX, wynikY, wynikRHO);

        fclose(plik);
        return true;
    }

    return false;
}

int main()
{
    char* sciezkDoPliku = "P0001_attr.rec.txt";

    int iloscElementow = zlicz_ilosc_wierszy_danych_w_pliku(sciezkDoPliku);

    if (iloscElementow == -1)
    {
        printf("Nie udalo sie wczytac danych z pliku '%s'.\n", sciezkDoPliku);
    }
    else
    {
        iloscElementow--; // -1 bo pomijam pierwszy wiersz naglowka

        if (iloscElementow < 1)
        {
            printf("Brak danych w pliku '%s'.\n", sciezkDoPliku);
        }
        else
        {
            struct DaneStatystyczne* dane = (struct DaneStatystyczne*)calloc(iloscElementow, sizeof(struct DaneStatystyczne));

            if (wczytaj_dane_z_pliku(sciezkDoPliku, dane))
            {
                printf("Dane z pliku:\n");

                float daneX[iloscElementow];
                float daneY[iloscElementow];
                float daneRHO[iloscElementow];

                for (int i = 0; i < iloscElementow; i++)
                {
                    struct DaneStatystyczne d = dane[i];
                    daneX[i] = d.X;
                    daneY[i] = d.Y;
                    daneRHO[i] = d.RHO;
                    printf("Lp %d. %f %f %f\n", i + 1, d.X, d.Y, d.RHO);
                }

                float wynikiX[3] = { oblicz_srednia(daneX, iloscElementow), oblicz_mediane(daneX, iloscElementow), oblicz_odchylenie(daneX, iloscElementow) };
                float wynikiY[3] = { oblicz_srednia(daneY, iloscElementow), oblicz_mediane(daneY, iloscElementow), oblicz_odchylenie(daneY, iloscElementow) };
                float wynikiRHO[3] = { oblicz_srednia(daneRHO, iloscElementow), oblicz_mediane(daneRHO, iloscElementow), oblicz_odchylenie(daneRHO, iloscElementow) };

                printf("\n\nSrednia X: %f\n", wynikiX[0]);
                printf("Mediana X: %f\n", wynikiX[1]);
                printf("Odchylenie X: %f\n\n", wynikiX[2]);

                printf("Srednia Y: %f\n", wynikiY[0]);
                printf("Mediana Y: %f\n", wynikiY[1]);
                printf("Odchylenie Y: %f\n\n", wynikiY[2]);

                printf("Srednia RHO: %f\n", wynikiRHO[0]);
                printf("Mediana RHO: %f\n", wynikiRHO[1]);
                printf("Odchylenie RHO: %f\n\n", wynikiRHO[2]);

                int lp = iloscElementow + 1;
                bool zapisano = false;

                for (int i = 0; i < 3; i++)
                {
                    float x = wynikiX[i];
                    float y = wynikiY[i];
                    float rho = wynikiRHO[i];

                    bool zapisac = true;

                    for (int j = 0; j < iloscElementow; j++)
                    {
                        struct DaneStatystyczne d = dane[j];
                        if (x == d.X && y == d.Y && rho == d.RHO)
                        {
                            zapisac = false;
                            break;
                        }
                    }

                    if (zapisac && zapisz_dane_do_pliku(sciezkDoPliku, x, y, rho, lp++))
                        zapisano = true;
                }

                if (zapisano)
                    printf("Zapisano wyniki do pliku '%s'.\n", sciezkDoPliku);
                else
                    printf("Nie zapisano wynikow do pliku '%s' poniewaz juz sie w nim znajduja.\n", sciezkDoPliku);
            }
            else
            {
                printf("Nie udalo sie wczytac danych z pliku '%s'.\n", sciezkDoPliku);
            }

            free(dane);
        }
    }

    return 0;
}
