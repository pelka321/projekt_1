#include "statystyka.h"

float oblicz_mediane(float* dane, int iloscElementow)
{
    for (int i = 0; i < iloscElementow; i++)
    {
        for (int j = 0; j < iloscElementow - 1; j++)
        {
            float wartosc1 = dane[j];
            float wartosc2 = dane[j + 1];
            if (wartosc1 > wartosc2)
            {
                dane[j] = wartosc2;
                dane[j + 1] = wartosc1;
            }
        }
    }

    int srodek = iloscElementow / 2;

    if (iloscElementow % 2 == 0)
    {
        return (dane[srodek] + dane[srodek - 1]) / 2.00;
    }

    return dane[srodek];
}
