#include "statystyka.h"

float oblicz_srednia(float* dane, int iloscElementow)
{
    float wynik = 0.0;

    for (int i = 0; i < iloscElementow; i++)
    {
        wynik += dane[i];
    }

    return wynik / iloscElementow;
}
