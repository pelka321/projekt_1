#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "statystyka.h"

int zlicz_ilosc_wierszy_danych_w_pliku(char* sciezkaDoPliku)
{
    FILE *plik = fopen(sciezkaDoPliku, "r");
    if (plik != NULL)
    {
        int wynik = 0;
        char linia[1000] = "";

        while (fgets(linia, 1000, plik) != NULL)
        {
            linia[strlen(linia) - 1] = '\0';

            // sprawdzenie czy we wczytanej linii tekstu sa w ogole jakies dane (pomijanie pustych linii)
            if (strlen(linia) > 0)
                wynik++;
        }

        fclose(plik);
        return wynik;
    }

    return -1;
}

bool wczytaj_dane_z_pliku(char* sciezkaDoPliku, float* tablicaX, float* tablicaY, float* tablicaRHO)
{
    FILE *plik = fopen(sciezkaDoPliku, "r");
    if (plik != NULL)
    {
        char sLp[10] = "";
        char sX[10] = "";
        char sY[10] = "";
        char sRHO[10] = "";

        if (fscanf(plik, "%s %s %s %s", sLp, sX, sY, sRHO) != EOF)
        {
            int i = 0;
            float x = 0.0;
            float y = 0.0;
            float rho = 0.0;

            while (fscanf(plik, "%s %f %f %f", sLp, &x, &y, &rho) != EOF)
            {
                if (strlen(sLp) > 0)
                {
                    tablicaX[i] = x;
                    tablicaY[i] = y;
                    tablicaRHO[i++] = rho;
                }
            }
        }
        fclose(plik);
        return true;
    }

    return false;
}

bool zapisz_dane_do_pliku(char* sciezkaDoPliku, float wynikiX[3], float wynikiY[3], float wynikiRHO[3], int lp)
{
    FILE *plik = fopen(sciezkaDoPliku, "r+");
    if (plik != NULL)
    {
        // Przejscie na koniec pliku.
        fseek(plik, 0, SEEK_END);

        // Cofniecie sie o jeden bajt w pliku, aby sprawdzic, czy ostatni znak w tym pliku to zlamanie linii (enter).
        fseek(plik, -1L, SEEK_CUR);

        // Jesli ostatni znak w pliku to zlamanie linii, to znowu cofniecie sie o jeden bajt,
        // poniewaz getc juz spowodowalo przejscie o jeden znak (bajt) do przodu. Cofniecie sie o znak
        // spowoduje nadpisanie entera enterem podczas pierwszego wpisu do pliku. Chodzi tu o to, aby
        // zawsze miec pewnosc, ze dane zostana wpisane do nowej linii, niezaleznie jak dane sie koncza
        // w pliku (enterem czy tez nie).
        if (getc(plik) == '\n')
        {
            fseek(plik, -1L, SEEK_CUR);
        }

        // Potwierdzenie biezacej pozycji w pliku.
        fseek(plik, 0, SEEK_CUR);

        for (int i = 0; i < 3; i++)
        {
            fprintf(plik, "\n%d.\t%.5f		%.5f	%.3f", ++lp, wynikiX[i], wynikiY[i], wynikiRHO[i]);
        }

        fclose(plik);
        return true;
    }

    return false;
}

int main()
{
    char* sciezkDoPliku = "P0001_attr.rec.txt";

    int iloscElementow = zlicz_ilosc_wierszy_danych_w_pliku(sciezkDoPliku);

    if (iloscElementow == -1)
    {
        printf("Nie udalo sie wczytac danych z pliku '%s'.\n", sciezkDoPliku);
    }
    else
    {
        iloscElementow--; // -1 bo pomijam pierwszy wiersz naglowka

        if (iloscElementow < 1)
        {
            printf("Brak danych w pliku '%s'.\n", sciezkDoPliku);
        }
        else
        {
            float* tablicaX = (float*)calloc(iloscElementow, sizeof(float));
            float* tablicaY = (float*)calloc(iloscElementow, sizeof(float));
            float* tablicaRHO = (float*)calloc(iloscElementow, sizeof(float));

            if (wczytaj_dane_z_pliku(sciezkDoPliku, tablicaX, tablicaY, tablicaRHO))
            {
                printf("Dane z pliku:\n");

                for (int i = 0; i < iloscElementow; i++)
                {
                    printf("Lp %d. %f %f %f\n", i + 1, tablicaX[i], tablicaY[i], tablicaRHO[i]);
                }

                float wynikiX[3] = { oblicz_srednia(tablicaX, iloscElementow), oblicz_mediane(tablicaX, iloscElementow), oblicz_odchylenie(tablicaX, iloscElementow) };
                float wynikiY[3] = { oblicz_srednia(tablicaY, iloscElementow), oblicz_mediane(tablicaY, iloscElementow), oblicz_odchylenie(tablicaY, iloscElementow) };
                float wynikiRHO[3] = { oblicz_srednia(tablicaRHO, iloscElementow), oblicz_mediane(tablicaRHO, iloscElementow), oblicz_odchylenie(tablicaRHO, iloscElementow) };

                printf("\n\nSrednia X: %f\n", wynikiX[0]);
                printf("Mediana X: %f\n", wynikiX[1]);
                printf("Odchylenie X: %f\n\n", wynikiX[2]);

                printf("Srednia Y: %f\n", wynikiY[0]);
                printf("Mediana Y: %f\n", wynikiY[1]);
                printf("Odchylenie Y: %f\n\n", wynikiY[2]);

                printf("Srednia RHO: %f\n", wynikiRHO[0]);
                printf("Mediana RHO: %f\n", wynikiRHO[1]);
                printf("Odchylenie RHO: %f\n\n", wynikiRHO[2]);

                if (zapisz_dane_do_pliku(sciezkDoPliku, wynikiX, wynikiY, wynikiRHO, iloscElementow))
                    printf("Pomyslnie zapisano dane do pliku '%s'.\n", sciezkDoPliku);
                else
                    printf("Nie udalo sie zapisac danych do pliku '%s'.\n", sciezkDoPliku);
            }
            else
            {
                printf("Nie udalo sie wczytac danych z pliku '%s'.\n", sciezkDoPliku);
            }

            free(tablicaX);
            free(tablicaY);
            free(tablicaRHO);
        }
    }

    return 0;
}
