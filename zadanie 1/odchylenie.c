#include "statystyka.h"
#include <math.h>

float oblicz_odchylenie(float* dane, int iloscElementow)
{
    float wynik = 0.0;
    float srednia = oblicz_srednia(dane, iloscElementow);

    for (int i = 0; i < iloscElementow; i++)
    {
        wynik += pow(dane[i] - srednia, 2);
    }

    wynik = wynik / iloscElementow;

    return sqrt(wynik);
}
